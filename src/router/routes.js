
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: '/curso-hebreo', component: () => import('pages/cursos/hebreo/index.vue') },
      { path: '/curso-arameo', component: () => import('pages/cursos/arameo/index.vue') },
      { path: '/curso-griego', component: () => import('pages/cursos/griego/index.vue') },
      { path: '/curso-latin', component: () => import('pages/cursos/latin/index.vue') },
    ]
  },
  // {
  //   path: '/cursos',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [
  //     { path: '', component: () => import('pages/hebreo/index.vue') },
  //   ]
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
